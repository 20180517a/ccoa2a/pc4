package com.cristhianwiki.pc4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class SecondActivity extends AppCompatActivity {
    EditText editTextDestinyName, editTextDirection;
    Spinner spinner;
    TextView textViewFoodValue;
    String destinyName, destinyDir;
    RadioGroup radioGroup;
    RadioButton radioButton1,radioButton2;
    LinearLayout linearLayout;
    Button button;
    String[] optionsQty = {"0", "1", "2", "3"};
    Boolean isVisa=false, isEffective=false;
    int qty;
    String food;
    SharedPreferences sharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        linearLayout = findViewById(R.id.linearLayout);
        textViewFoodValue = findViewById(R.id.text_view_food_value);
        editTextDestinyName = findViewById(R.id.edit_text_destiny_name);
        editTextDirection = findViewById(R.id.edit_text_direction);
        radioGroup = findViewById(R.id.radio_group);
        radioButton1 = findViewById(R.id.radio_button_1);
        radioButton2 = findViewById(R.id.radio_button_2);
        button = findViewById(R.id.button);

        spinner = findViewById(R.id.spinner);
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, optionsQty);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                qty = i;
                return;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        button.setOnClickListener(v -> {
            if(     radioButton1.isChecked() == false &&
                    radioButton2.isChecked() == false &&
                    destinyName == "" &&
                    destinyDir == ""
            ){
                Snackbar.make(linearLayout, R.string.msg_snackbar, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.button_snackbar_text, v1 -> {

                        }).show();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder
                        .setTitle(R.string.dialog_title)
                        .setMessage(R.string.dialog_msg)
                        .setPositiveButton(R.string.yes, (dialog, which) -> {

                        })
                        .setNegativeButton(R.string.no, (dialog, which) -> dialog.cancel()).show();
            }
        });

        Intent intent = getIntent();
        String text = intent.getStringExtra("FOOD");

        textViewFoodValue.setText(text);

        retrieveData();
    }


    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    @Override
    public void onBackPressed() {
        sharedPreference = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString("key food", null);
    }

    private void saveData() {
        sharedPreference = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        destinyName = editTextDestinyName.getText().toString();
        destinyDir = editTextDirection.getText().toString();
        food = textViewFoodValue.getText().toString();

        if(radioButton1.isChecked()){
            isVisa = true;
            isEffective = false;
        }
        if(radioButton2.isChecked()){
            isEffective = true;
            isVisa = false;
        }

        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString("key food", food);
        editor.putString("key destinyName", destinyName);
        editor.putString("key destinyDir", destinyDir);
        editor.putInt("key qty", qty);
        editor.putBoolean("key isVisa", isVisa);
        editor.putBoolean("key isEffective", isEffective);
        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos están guardados", Toast.LENGTH_LONG).show();
    }

    private void retrieveData() {
        sharedPreference = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        destinyName = sharedPreference.getString("key destinyName", null);
        destinyDir = sharedPreference.getString("key destinyDir", null);
        qty = sharedPreference.getInt("key qty", 0);
        isEffective = sharedPreference.getBoolean("key isEffective", false);
        isVisa = sharedPreference.getBoolean("key isVisa", false);
        food = sharedPreference.getString("key food", null);

        radioButton1.setChecked(isVisa);
        radioButton2.setChecked(isEffective);
        editTextDestinyName.setText(destinyName);
        editTextDirection.setText(destinyDir);
        //textViewFoodValue.setText(food);
    }
}
