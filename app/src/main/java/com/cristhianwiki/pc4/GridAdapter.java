package com.cristhianwiki.pc4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class GridAdapter extends BaseAdapter {
    Context context;
    List<String> titles;
    List<String> descriptions;
    List<Integer> images;

    public GridAdapter(Context context, List<String> titles, List<String> descriptions, List<Integer> images){
        this.context = context;
        this.titles = titles;
        this.descriptions = descriptions;
        this.images = images;
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_layout,parent,false);
        ImageView imageView = view.findViewById(R.id.image_view);
        TextView textViewTitle = view.findViewById(R.id.tv_title);
        TextView textViewDescription = view.findViewById(R.id.tv_description);
        imageView.setImageResource(images.get(position));
        textViewTitle.setText(titles.get(position));
        textViewDescription.setText(descriptions.get(position));
        return view;
    }
}
