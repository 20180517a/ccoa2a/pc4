package com.cristhianwiki.pc4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
import android.content.Intent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    List<String> titles = new ArrayList<>();
    List<String> descriptions = new ArrayList<>();
    List<Integer> images = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intentPast = getIntent();
        String food = intentPast.getStringExtra("FOOD");

        //if()
        gridView = findViewById(R.id.grid_view);
        fillArray();
        GridAdapter adapter = new GridAdapter(this, titles, descriptions, images);
        gridView.setAdapter(adapter);

        //gridView.setOnItemClickListener((parent, view, position, id) ->
        //        Toast.makeText(this, titles.get(position), Toast.LENGTH_SHORT).show());
        gridView.setOnItemClickListener((parent, view, position, id) -> {
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("FOOD", titles.get(position));
                    startActivity(intent);
                });
                //Toast.makeText(this, names.get(position), Toast.LENGTH_SHORT).show());
    }

    private void fillArray(){
        titles.add("Ceviche");
        titles.add("Escabeche");
        titles.add("Lomo");
        titles.add("Mazamorra");
        titles.add("Seco");
        titles.add("Sopa seca");
        titles.add("Tallarines rojos");
        titles.add("Tallarines verdes");

        descriptions.add("Hecho con pescado");
        descriptions.add("Hecho con cebolla y pollo");
        descriptions.add("Hecho con carne de res");
        descriptions.add("Hecho con maiz morado");
        descriptions.add("Hecho con cabrito");
        descriptions.add("Tiene tomate, zanahoria y pollo");
        descriptions.add("Hecho con tomates selectos");
        descriptions.add("Hecho con albaca");

        images.add(R.drawable.ceviche);
        images.add(R.drawable.escabeche);
        images.add(R.drawable.lomo_saltado);
        images.add(R.drawable.mazamorra);
        images.add(R.drawable.seco);
        images.add(R.drawable.sopa_seca);
        images.add(R.drawable.tallarin_rojo);
        images.add(R.drawable.tallarin_verde);
    }
}
